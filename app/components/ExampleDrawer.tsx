"use client";
import { useState } from "react";
import { Loader, LoadingTile, PersonTile } from ".";

export const ExampleDrawer = () => {
  const [shown, setShown] = useState<boolean>(false);
  return (
    <>
      <div className="text-end">
        <button
          className="overflow-y-auto text-white bg-black  focus:ring-4 focus:ring-blue-300 
          font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-blue-900 
          focus:outline-none dark:focus:ring-blue-800"
          type="button"
          onClick={() => setShown(!shown)}
        >
          Show Example Components
        </button>
      </div>
      <div
        id="drawer-top-example"
        className={`fixed top-0 left-0 right-0 z-40 w-full p-4 transition-transform ${
          !shown && "translate-x-full"
        } bg-white dark:bg-slate-900 `}
      >
        <button
          type="button"
          onClick={() => setShown(!shown)}
          data-drawer-hide="drawer-top-example"
          aria-controls="drawer-top-example"
          className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 absolute top-2.5 end-2.5 inline-flex items-center justify-center dark:hover:bg-gray-600 dark:hover:text-white"
        >
          <svg
            className="w-3 h-3"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 14"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
            />
          </svg>
        </button>
        <div className="flex gap-4 p-2">
          {/* //The components are being rendered here */}
          <div>
            Loader
            <Loader />
          </div>
          <div>
            Loading Tile
            <LoadingTile />
          </div>
          <div>
            PersonTile
            <PersonTile
              name={"name"}
              address="address"
              phone={"phone number"}
            />
          </div>
        </div>
      </div>
    </>
  );
};
