"use client";

// All the components being imported
import { Loader, LoadingTile, PersonTile } from "./components";
// The hook to get data from the endpoint
import { useGetDataFromEndpoint } from "./components/hooks/useGetDataFromEndpoint";
// The types
import { PeopleIds, Person } from "./types";

/**
 * endpoints
 * - https://example-one-test.vercel.app/api/people/getAllPeople
 * Returns a list of people id's in the type of PeopleIds
 *
 * - https://example-one-test.vercel.app/api/people/getPerson/{personId}
 * Returns a person in the type of Person
 */

export default function Primary() {
  // Your code here
  // Use the useGetDataFromEndpoint hook to fetch all data
  // note the types PeopleIds and Person, use them to type your data
  return (
    <div className="flex gap-4 p-2 w-full flex-wrap">
      {/*Your JSX here: The div wrapper will already have flex and wrapping so you only need to create the tiles*/}
    </div>
  );
}
