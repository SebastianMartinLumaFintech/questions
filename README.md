### Task

1. Leverage the existing Components, and the custom hook to create a page that looks and functions like the solved page.
1. The solved page is available at `https://example-one-test.vercel.app/done/1`

### Notes

1. This is a Next.js application. You do not need to know Next.js to complete this task.
1. If you are not familiar with Next.js, you can treat this as a React application, I can guide you if you have any problems.
1. If you run into any issues, please ask questions. We are here to help.
1. If you have an AI assistant, please do not use it for this task.
1. If you need to look up documentation, please do so. We are looking for a solution that works, not a solution that is memorized.
1. However please let us see what you are looking at, so we can help you if you are looking at the wrong thing.

### Instructions

1. Run the application in development mode, there is a npm script available
1. Look at the served page at `http://localhost:3000`
1. Look at the example components on the page in the Show Example Components Button, this will show you the components that you will be using.

1. Look through the existing component files
   1. Loader - a loader component (`app/components/Loader.tsx`)
   1. LoadingTile - a loading tile component (`app/components/LoadingTile.tsx`)
   1. PersonTile - a person tile component (`app/components/PersonTile.tsx`)
1. To see an example useage of these components please look at ExampleDrawer (`app/components/ExampleDrawer.tsx`) (lines 50-67)

1. Look at the existing hook

   1. useGetDataFromEndpoint - a hook that fetches data from an endpoint (`app/components/hooks/useGetDataFromEndpoint.tsx`)

1. There are 2 endpoints to be used for this task

   1. https://example-one-test.vercel.app/api/people/getAllPeople
      1. This endpoint returns a list of people in the type of PeopleIds (`app/types/index.ts`)
   1. https://example-one-test.vercel.app/api/people/getPerson/{personId}
      1. This endpoint returns a person in the type of Person(`app/types/index.ts`)

1. Open app/page.tsx, this is the entrypoint into the application.

   1. Note all of the things that are imported at the top of the file.
   1. YOU WILL START YOUR CODE IN THIS FILE: (`app/page.tsx`)

1. ### Based on all these components and the hook, create a page that looks like the solved page.
   1. Please use the hook to fetch all data, and use the components to display the data.
   1. Feel free to create any new components or hooks that you feel are necessary.
   1. Please do not use any external libraries to complete this task.
